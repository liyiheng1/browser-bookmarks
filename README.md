











### February 25, 2021 
- [Why Not Rust?](https://matklad.github.io/2020/09/20/why-not-rust.html) 
### February 19, 2021 
- [B站新一代golang规则引擎的设计与实现 - InfoQ 写作平台](https://xie.infoq.cn/article/40bfff1fbca1867991a1453ac) 
- [18张图解密新时代内存分配器TCMalloc](https://juejin.cn/post/6924899045353881608) 
### February 1, 2021 
- [云原生数据库设计新思路 | PingCAP](https://pingcap.com/blog-cn/new-ideas-for-designing-cloud-native-database/) 
### January 27, 2021 
- [Go: nil == nil is true or false? Na tum jano na hum…](https://medium.com/@shivi28/go-when-nil-nil-returns-true-a8a014abeffb) 
### January 26, 2021 
- [别再问我们用什么画图的了！问就是 excalidraw · GoCN社区](https://gocn.vip/topics/11562) 
### January 24, 2021 
- [C++ moves for people who don’t know or care what rvalues are](https://medium.com/@winwardo/c-moves-for-people-who-dont-know-or-care-what-rvalues-are-%EF%B8%8F-56ee122dda7) 
### January 22, 2021 
- [Pure functions in C++. Add attributes to declare a function as… | by EventHelix | Software Design | Medium](https://medium.com/software-design/pure-functions-in-c-fc102fd9c5e0) 
### January 21, 2021 
- [MuscleWiki](https://musclewiki.com/) 
### January 20, 2021 
- [Stanford CS193u: Video Game Development in C++ and Unreal Engine – Tom Looman](https://www.tomlooman.com/stanford-cs193u/) 
### January 18, 2021 
- [A Quiz of Equal Operator in Go. Basic equality comparison in Go](https://wexort.medium.com/a-quiz-of-equal-operator-in-go-c2e34f130cfc) 
### January 13, 2021 
- [Go and CPU Caches. How understanding the processor… | by Teiva Harsanyi | Medium](https://teivah.medium.com/go-and-cpu-caches-af5d32cc5592) 
- [Mechanical Sympathy](https://mechanical-sympathy.blogspot.com/) 
- [Go 大数据生态开源项目 CDS 中 ClickHouse 使用的建表方案 · GoCN社区](https://gocn.vip/topics/11306) 
- [Clear is better than clever – The acme of foolishness](https://dave.cheney.net/2019/07/09/clear-is-better-than-clever) 
### January 12, 2021 
- [Go 运行程序中的线程数 | 鸟窝](https://colobu.com/2020/12/20/threads-in-go-runtime/) 
- [5 Secure Coding Tips in Go. | by Mert Akkaya| Medium](https://mert-akkaya.medium.com/5-secure-coding-tips-in-go-a3e5ec23d7fd) 
### January 5, 2021 
- [Query and filter context | Elasticsearch Reference [7.10] | Elastic](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-filter-context.html) 
- [Paginate search results | Elasticsearch Reference [7.10] | Elastic](https://www.elastic.co/guide/en/elasticsearch/reference/current/paginate-search-results.html) 
